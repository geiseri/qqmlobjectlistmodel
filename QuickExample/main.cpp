#include "person.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <qqmlobjectlist.h>
#include <qqmlobjectlistmodel.h>

int main(int argc, char* argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<Person>("TestData", 1, 0, "Person");
    qmlRegisterUncreatableType<QQmlObjectListModel>("Geekcentral.Models", 1, 0, "QQmlObjectListModel", QLatin1String("QQmlObjectListModel can only be exposed as a property"));

    QQmlObjectList<Person> model;
    model.append(new Person("Ann", 10));
    model.append(new Person("Pete", 11));
    model.append(new Person("Joe", 12));
    model.append(new Person("Bob", 13));

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("People", &model);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
