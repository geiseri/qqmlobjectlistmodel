#ifndef PERSON_H
#define PERSON_H

#include <QObject>

class Person : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(quint32 age READ age WRITE setAge NOTIFY ageChanged)

public:
    explicit Person(QObject* parent = 0);
    Person(const QString& name, int age, QObject* parent = 0);

    QString name() const;
    void setName(const QString& name);

    quint32 age() const;
    void setAge(const quint32& age);

signals:
    void nameChanged();
    void ageChanged();

private:
    QString m_name;
    quint32 m_age;
};

#endif // PERSON_H
