import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import Geekcentral.Models 1.0
import TestData 1.0

Window {
    visible: true
    width: 300
    height: 200
    title: qsTr("Hello World")

    ListView {
        anchors.fill: parent
        id: view
        model: People
        delegate: Text { text: name }
        Layout.fillHeight: true
        Layout.fillWidth: true
    }


    Component.onCompleted: {
        for( var idx = 0; idx < People.length; idx++) {
            console.log("Name: %1 Age: %2".arg(People.at(idx).name).arg(People.at(idx).age))
        }
    }

}
