#include "person.h"

Person::Person(QObject* parent)
    : QObject(parent)
    , m_age(1)
{
}

Person::Person(const QString& name, int age, QObject* parent)
    : QObject(parent)
    , m_name(name)
    , m_age(age)
{
}

QString Person::name() const
{
    return m_name;
}

void Person::setName(const QString& name)
{
    if (m_name == name)
        return;

    m_name = name;
    Q_EMIT nameChanged();
}

quint32 Person::age() const
{
    return m_age;
}

void Person::setAge(const quint32& age)
{
    if (m_age == age)
        return;

    m_age = age;
    Q_EMIT ageChanged();
}
