#include "qqmllistmodel_plugin.h"
#include "qqmlobjectlistmodel.h"

#include <qqml.h>

void QQmlListModelPlugin::registerTypes(const char* uri)
{
    // @uri Geekcentral.Models
    qmlRegisterUncreatableType<QQmlObjectListModel>(uri, 1, 0, "QQmlObjectListModel", QLatin1String("QQmlObjectListModel can only be exposed as a property"));
}
