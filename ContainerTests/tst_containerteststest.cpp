#include <QCoreApplication>
#include <QSignalSpy>
#include <QString>
#include <QtTest>
#include <qqmlobjectlist.h>

class Person : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int age READ age WRITE setAge NOTIFY ageChanged)
public:
    Person(const QString& name, int age, QObject* parent = 0)
        : QObject(parent)
        , m_name(name)
        , m_age(age)
    {
    }

    QString name() const
    {
        return m_name;
    }

    void setName(const QString& name)
    {
        if (m_name == name)
            return;
        m_name = name;
        Q_EMIT nameChanged();
    }

    int age() const
    {
        return m_age;
    }

    void setAge(int age)
    {
        if (m_age == age)
            return;
        m_age = age;
        Q_EMIT ageChanged();
    }

signals:
    void nameChanged();
    void ageChanged();

private:
    QString m_name;
    int m_age;
};

class ContainerTestsTest : public QObject {
    Q_OBJECT

public:
    ContainerTestsTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testInserts();
    void testChangeMember();
    void testRemoves();
    void testInitializerList();
    void testHandlerDisconnect();
    void testExternalDelete();
};

ContainerTestsTest::ContainerTestsTest()
{
}

void ContainerTestsTest::initTestCase()
{
}

void ContainerTestsTest::cleanupTestCase()
{
}

void ContainerTestsTest::testInserts()
{
    QQmlObjectList<Person> people;
    QSignalSpy spy(&people, SIGNAL(rowsInserted(QModelIndex, int, int)));

    people.append(new Person("Ann", 25));
    people.prepend(new Person("Joe", 15));
    people.insert(1, new Person("Pete", 12));
    people.insert(2, new Person("Frank", 30));

    QCOMPARE(people.at(0)->name(), QString("Joe"));
    QCOMPARE(people.at(1)->name(), QString("Pete"));
    QCOMPARE(people.at(2)->name(), QString("Frank"));
    QCOMPARE(people.at(3)->name(), QString("Ann"));
    QCOMPARE(spy.count(), 4);

    QCOMPARE(spy.at(0).at(1), QVariant::fromValue(0));
    QCOMPARE(spy.at(0).at(2), QVariant::fromValue(0));

    QCOMPARE(spy.at(1).at(1), QVariant::fromValue(0));
    QCOMPARE(spy.at(1).at(2), QVariant::fromValue(0));

    QCOMPARE(spy.at(2).at(1), QVariant::fromValue(1));
    QCOMPARE(spy.at(2).at(2), QVariant::fromValue(1));

    QCOMPARE(spy.at(3).at(1), QVariant::fromValue(2));
    QCOMPARE(spy.at(3).at(2), QVariant::fromValue(2));
}

void ContainerTestsTest::testChangeMember()
{
    QQmlObjectList<Person> people;
    QSignalSpy spy(&people, SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)));

    people.append(new Person("Ann", 25));
    people.append(new Person("Joe", 15));
    people.append(new Person("Pete", 12));

    people.at(1)->setAge(30);

    QCOMPARE(people.at(1)->age(), 30);
    QCOMPARE(spy.count(), 1);
    QCOMPARE(spy.at(0).at(2).value<QVector<int> >(), QVector<int>({ 258 }));
}

void ContainerTestsTest::testRemoves()
{
    QQmlObjectList<Person> people;
    QSignalSpy spy(&people, SIGNAL(rowsRemoved(QModelIndex, int, int)));

    people.append(new Person("Ann", 25));
    people.append(new Person("Joe", 15));
    people.append(new Person("Pete", 12));
    people.append(new Person("Pat", 18));
    people.append(new Person("Bob", 30));

    delete people.takeFirst(); // remove 0
    delete people.takeLast(); // remove 3
    delete people.takeAt(1); // remove 1

    QCOMPARE(people.at(0)->name(), QString("Joe"));
    QCOMPARE(people.at(1)->name(), QString("Pat"));
    QCOMPARE(spy.count(), 3);

    QCOMPARE(spy.at(0).at(1), QVariant::fromValue(0));
    QCOMPARE(spy.at(0).at(2), QVariant::fromValue(0));

    QCOMPARE(spy.at(1).at(1), QVariant::fromValue(3));
    QCOMPARE(spy.at(1).at(2), QVariant::fromValue(3));

    QCOMPARE(spy.at(2).at(1), QVariant::fromValue(1));
    QCOMPARE(spy.at(2).at(2), QVariant::fromValue(1));
}

void ContainerTestsTest::testInitializerList()
{

#ifdef Q_COMPILER_INITIALIZER_LISTS
    QQmlObjectList<Person> people({ new Person("Ann", 25),
        new Person("Joe", 16),
        new Person("Pete", 15) });
    QSignalSpy spy(&people, SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)));
    people.at(1)->setAge(30);

    QCOMPARE(people.at(1)->age(), 30);
    QCOMPARE(spy.count(), 1);
    QCOMPARE(spy.at(0).at(2).value<QVector<int> >(), QVector<int>({ 258 }));
#endif
}

void ContainerTestsTest::testHandlerDisconnect()
{
    QQmlObjectList<Person> people;
    QSignalSpy spy(&people, SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)));

    people.append(new Person("Ann", 25));
    people.append(new Person("Joe", 15));
    people.append(new Person("Pete", 12));

    Person* p1 = people.takeAt(1);

    p1->setAge(30);

    delete p1;

    QCOMPARE(spy.count(), 0);
}

void ContainerTestsTest::testExternalDelete()
{
    QQmlObjectList<Person> people;
    QSignalSpy spy(&people, SIGNAL(rowsRemoved(QModelIndex, int, int)));

    people.append(new Person("Ann", 25));
    people.append(new Person("Joe", 15));
    people.append(new Person("Pete", 12));
    people.append(new Person("Pat", 18));
    people.append(new Person("Bob", 30));

    delete people.at(3);

    QCOMPARE(people.at(3)->name(), QString("Bob"));
    QCOMPARE(spy.count(), 1);

    QCOMPARE(spy.at(0).at(1), QVariant::fromValue(3));
    QCOMPARE(spy.at(0).at(2), QVariant::fromValue(3));
}

QTEST_MAIN(ContainerTestsTest)

#include "tst_containerteststest.moc"
