#ifndef QQMLOBJECTLISTMODEL_H
#define QQMLOBJECTLISTMODEL_H

#include <QAbstractListModel>
#include <QJSValue>
#include <QList>
#include <QMetaObject>

class QQmlObjectListModel : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY(int length READ length NOTIFY lengthChanged)

public:
    explicit QQmlObjectListModel(QObject* parent = 0);
    virtual ~QQmlObjectListModel();

    // QML interface
public:
    virtual int length() const;
    Q_INVOKABLE virtual bool contains(QObject* item) const;
    Q_INVOKABLE virtual int indexOf(QObject* item) const;
    Q_INVOKABLE virtual void clear();
    Q_INVOKABLE virtual void append(QObject* item);
    Q_INVOKABLE virtual void prepend(QObject* item);
    Q_INVOKABLE virtual void insert(int idx, QObject* item);
    Q_INVOKABLE virtual void move(int idx, int pos);
    Q_INVOKABLE virtual void remove(QObject* item);
    Q_INVOKABLE virtual void remove(int idx);
    Q_INVOKABLE virtual QObject* at(int index) const;
    Q_INVOKABLE virtual QObject* first() const;
    Q_INVOKABLE virtual QObject* last() const;

    // QAbstractListModel interface
public:
    int rowCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void lengthChanged();

private Q_SLOTS:
    void handleItemPropertyChanged();

protected:
    void setupMetaObject(const QMetaObject* obj);
    void configureObject(QObject* obj);
    void deconfigureObject(QObject* obj);

    // Internal helpers
    virtual int _p_length() const = 0;
    virtual QObject* _p_at(int index) const = 0;
    virtual QObject* _p_first() const = 0;
    virtual QObject* _p_last() const = 0;
    virtual int _p_indexOf(QObject* const& item) const = 0;
    virtual void _p_clear() = 0;
    virtual void _p_append(QObject* item) = 0;
    virtual void _p_prepend(QObject* item) = 0;
    virtual void _p_insert(int idx, QObject* item) = 0;
    virtual void _p_move(int idx, int pos) = 0;
    virtual void _p_remove(int idx) = 0;
    virtual void _p_purge(QObject* obj) = 0;

private:
    QHash<int, int> m_signalRoleMap;
    QHash<int, QByteArray> m_roles;
};

#endif // QQMLOBJECTLISTMODEL_H
