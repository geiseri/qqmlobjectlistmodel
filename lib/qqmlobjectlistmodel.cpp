#include "qqmlobjectlistmodel.h"
#include <QDebug>
#include <QQmlEngine>

QQmlObjectListModel::QQmlObjectListModel(QObject* parent)
    : QAbstractListModel(parent)
{
    connect(this, &QAbstractItemModel::rowsInserted, this, &QQmlObjectListModel::lengthChanged);
    connect(this, &QAbstractItemModel::rowsRemoved, this, &QQmlObjectListModel::lengthChanged);
    connect(this, &QAbstractItemModel::modelReset, this, &QQmlObjectListModel::lengthChanged);
}

QQmlObjectListModel::~QQmlObjectListModel()
{
}

int QQmlObjectListModel::length() const
{
    return _p_length();
}

QObject* QQmlObjectListModel::at(int index) const
{
    return _p_at(index);
}

bool QQmlObjectListModel::contains(QObject* item) const
{
    return _p_indexOf(item) != -1;
}

int QQmlObjectListModel::indexOf(QObject* item) const
{
    return _p_indexOf(item);
}

void QQmlObjectListModel::clear()
{
    _p_clear();
}

void QQmlObjectListModel::append(QObject* item)
{
    _p_append(item);
}

void QQmlObjectListModel::prepend(QObject* item)
{
    _p_prepend(item);
}

void QQmlObjectListModel::insert(int idx, QObject* item)
{
    _p_insert(idx, item);
}

void QQmlObjectListModel::move(int idx, int pos)
{
    _p_move(idx, pos);
}

void QQmlObjectListModel::remove(QObject* item)
{
    _p_remove(_p_indexOf(item));
}

void QQmlObjectListModel::remove(int idx)
{
    _p_remove(idx);
}

QObject* QQmlObjectListModel::first() const
{
    return _p_first();
}

QObject* QQmlObjectListModel::last() const
{
    return _p_last();
}

int QQmlObjectListModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return length();
}

QVariant QQmlObjectListModel::data(const QModelIndex& index, int role) const
{
    if (index.row() < rowCount() && m_roles.contains(role)) {
        QByteArray prop = m_roles[role];
        return at(index.row())->property(prop.constData());
    }

    return QVariant();
}

bool QQmlObjectListModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (index.row() < rowCount() && m_roles.contains(role)) {
        QByteArray prop = m_roles[role];
        return at(index.row())->setProperty(prop.constData(), value);
    }

    return false;
}

QHash<int, QByteArray> QQmlObjectListModel::roleNames() const
{
    return m_roles;
}

void QQmlObjectListModel::handleItemPropertyChanged()
{
    QObject* source = sender();
    if (source) {
        int role = m_signalRoleMap[senderSignalIndex()];
        int row = indexOf(source);
        Q_EMIT dataChanged(index(row, 0), index(row, 0), QVector<int>({ role }));
    }
}

void QQmlObjectListModel::configureObject(QObject* obj)
{
    QQmlEngine::setObjectOwnership(obj, QQmlEngine::CppOwnership);

    const QMetaMethod slot = metaObject()->method(metaObject()->indexOfMethod("handleItemPropertyChanged()"));
    const QMetaObject* metaObj = obj->metaObject();
    for (int idx = 0; idx < metaObj->propertyCount(); idx++) {
        const QMetaProperty prop = metaObj->property(idx);
        if (prop.hasNotifySignal()) {
            connect(obj, prop.notifySignal(), this, slot, Qt::UniqueConnection);
        }
    }
    // cleanup handler
    connect(obj, &QObject::destroyed, this, &QQmlObjectListModel::_p_purge);
}

void QQmlObjectListModel::deconfigureObject(QObject* obj)
{
    const QMetaMethod slot = metaObject()->method(metaObject()->indexOfMethod("handleItemPropertyChanged()"));
    const QMetaObject* metaObj = obj->metaObject();
    for (int idx = 0; idx < metaObj->propertyCount(); idx++) {
        const QMetaProperty prop = metaObj->property(idx);
        if (prop.hasNotifySignal()) {
            disconnect(obj, prop.notifySignal(), this, slot);
        }
    }
    // cleanup handler
    disconnect(obj, &QObject::destroyed, this, &QQmlObjectListModel::_p_purge);
}

void QQmlObjectListModel::setupMetaObject(const QMetaObject* obj)
{

    for (int idx = 0; idx < obj->propertyCount(); idx++) {
        const QMetaProperty prop = obj->property(idx);
        if (prop.hasNotifySignal()) {
            m_roles[Qt::UserRole + idx] = prop.name();
            m_signalRoleMap[prop.notifySignalIndex()] = Qt::UserRole + idx;
        }
    }
}
