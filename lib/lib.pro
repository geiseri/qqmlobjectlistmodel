TEMPLATE = lib
TARGET = QQmlListModel
TARGET = $$qtLibraryTarget($$TARGET)
QT -= gui
QT += qml

CONFIG += c++11
CONFIG += create_pc create_prl no_install_prl

VER_MAJ = 0
VER_MIN = 1
VER_PAT = 0

!defined( PREFIX, var ) {
    PREFIX=/usr
}

target.path = $${PREFIX}/lib
INSTALLS += target

headers.files = $${HEADERS}
headers.path = $${PREFIX}/include/$${TARGET}
INSTALLS += headers

QMAKE_PKGCONFIG_NAME = QQmlListModel
QMAKE_PKGCONFIG_DESCRIPTION = Library to make a Model out of QObjects
QMAKE_PKGCONFIG_PREFIX = $${PREFIX}
QMAKE_PKGCONFIG_LIBDIR = $${target.path}
QMAKE_PKGCONFIG_INCDIR = $${headers.path}
QMAKE_PKGCONFIG_VERSION = $${VER_MAJ}.$${VER_MIN}.$${VER_PAT}
QMAKE_PKGCONFIG_CFLAGS += -std=c++0x
QMAKE_PKGCONFIG_REQUIRES = Qt5Qml
QMAKE_PKGCONFIG_DESTDIR = pkgconfig

HEADERS += \
    qqmlobjectlist.h \
    qqmlobjectlistmodel.h

SOURCES += \
    qqmlobjectlistmodel.cpp

