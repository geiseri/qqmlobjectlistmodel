#ifndef QQMLOBJECTLIST_H
#define QQMLOBJECTLIST_H
#include <qqmlobjectlistmodel.h>

template <typename T>
class QQmlObjectList : public QQmlObjectListModel {
public:
    explicit QQmlObjectList(QObject* parent = 0)
        : QQmlObjectListModel(parent)
    {
        setupMetaObject(&T::staticMetaObject);
    }

#ifdef Q_COMPILER_INITIALIZER_LISTS
    inline QQmlObjectList(std::initializer_list<T*> args, QObject* parent = 0)
        : QQmlObjectListModel(parent)
        , m_items(args)
    {
        setupMetaObject(&T::staticMetaObject);
        for (T* itm : m_items) {
            configureObject(itm);
        }
    }
#endif

    virtual ~QQmlObjectList()
    {
        for (T* obj : m_items) {
            deconfigureObject(obj);
            obj->deleteLater();
        }
    }

    // QList<...> interface
public:
    void append(T* const& item)
    {
        beginInsertRows(QModelIndex(), m_items.length(), m_items.length());
        m_items.append(item);
        configureObject(item);
        endInsertRows();
    }

    T* at(int idx) const
    {
        return m_items.at(idx);
    }

    T* back() const
    {
        return m_items.back();
    }

    void clear()
    {
        beginResetModel();

        for (T* obj : m_items) {
            deconfigureObject(obj);
            obj->deleteLater();
        }
        m_items.clear();

        endResetModel();
    }

    bool contains(T* const& value) const
    {
        return m_items.contains(value);
    }

    int count() const
    {
        return m_items.count();
    }

    bool empty() const
    {
        return m_items.empty();
    }

    bool endsWith(T* const& value) const
    {
        return m_items.endsWith(value);
    }

    T* first() const
    {
        return m_items.first();
    }

    T* front() const
    {
        return m_items.front();
    }

    int indexOf(T* const& value, int from = 0) const
    {
        return m_items.indexOf(value, from);
    }

    void insert(int idx, T* const& value)
    {
        beginInsertRows(QModelIndex(), idx, idx);
        m_items.insert(idx, value);
        configureObject(value);
        endInsertRows();
    }

    bool isEmpty() const
    {
        return m_items.isEmpty();
    }

    T* last() const
    {
        return m_items.last();
    }

    int lastIndexOf(T* const& value, int from = -1) const
    {
        return m_items.lastIndexOf(value, from);
    }

    void move(int from, int to)
    {
        beginMoveRows(QModelIndex(), from, from, QModelIndex(), to);
        m_items.move(from, to);
        endMoveRows();
    }

    void pop_back()
    {
        removeLast();
    }

    void pop_front()
    {
        removeFirst();
    }

    void prepend(T* const& value)
    {
        beginInsertRows(QModelIndex(), 0, 0);
        m_items.prepend(value);
        configureObject(value);
        endInsertRows();
    }

    void push_back(T* const& value)
    {
        append(value);
    }

    void push_front(T* const& value)
    {
        prepend(value);
    }

    int removeAll(T* const& value)
    {
        int num = 0;
        while (removeOne(value)) {
            num++;
        }
        return num;
    }

    void removeAt(int idx)
    {
        takeAt(idx)->deleteLater();
    }

    void removeFirst()
    {
        takeFirst()->deleteLater();
    }

    void removeLast()
    {
        takeLast()->deleteLater();
    }

    bool removeOne(T* const& value)
    {
        int idx = m_items.indexOf(value);
        if (idx != -1) {
            removeAt(idx);
            return true;
        } else {
            return false;
        }
    }

    void replace(int idx, T* const& value)
    {
        T* obj = at(idx);
        deconfigureObject(obj);
        obj->deleteLater();
        m_items.replace(idx, value);
        Q_EMIT dataChanged(QModelIndex(idx, 0), QModelIndex(idx, 0));
    }

    int size() const
    {
        return m_items.size();
    }

    bool startsWith(T* const& value) const
    {
        return m_items.startsWith(value);
    }

    T* takeAt(int idx)
    {
        beginRemoveRows(QModelIndex(), idx, idx);
        T* obj = m_items.takeAt(idx);
        deconfigureObject(obj);
        endRemoveRows();
        return obj;
    }

    T* takeFirst()
    {
        beginRemoveRows(QModelIndex(), 0, 0);
        T* obj = m_items.takeFirst();
        deconfigureObject(obj);
        endRemoveRows();
        return obj;
    }

    T* takeLast()
    {
        beginRemoveRows(QModelIndex(), count() - 1, count() - 1);
        T* obj = m_items.takeLast();
        deconfigureObject(obj);
        endRemoveRows();
        return obj;
    }

    T* value(int idx) const
    {
        return m_items.value(idx);
    }

    T* value(int idx, T* const& defaultValue) const
    {
        return m_items.value(idx, defaultValue);
    }

    typedef typename QList<T*>::const_iterator const_iterator;
    const_iterator begin() const
    {
        return m_items.begin();
    }
    const_iterator end() const
    {
        return m_items.end();
    }
    const_iterator constBegin() const
    {
        return m_items.constBegin();
    }
    const_iterator constEnd() const
    {
        return m_items.constEnd();
    }

protected:
    virtual int _p_length() const Q_DECL_OVERRIDE
    {
        return count();
    }

    virtual QObject* _p_at(int index) const Q_DECL_OVERRIDE
    {
        return at(index);
    }

    virtual QObject* _p_first() const Q_DECL_OVERRIDE
    {
        return first();
    }

    virtual QObject* _p_last() const Q_DECL_OVERRIDE
    {
        return last();
    }

    virtual int _p_indexOf(QObject* const& item) const Q_DECL_OVERRIDE
    {
        return indexOf(qobject_cast<T* const>(item));
    }

    virtual void _p_clear() Q_DECL_OVERRIDE
    {
        clear();
    }

    virtual void _p_append(QObject* item) Q_DECL_OVERRIDE
    {
        append(qobject_cast<T*>(item));
    }

    virtual void _p_prepend(QObject* item) Q_DECL_OVERRIDE
    {
        prepend(qobject_cast<T*>(item));
    }

    virtual void _p_insert(int idx, QObject* item) Q_DECL_OVERRIDE
    {
        insert(idx, qobject_cast<T*>(item));
    }

    virtual void _p_move(int idx, int pos) Q_DECL_OVERRIDE
    {
        move(idx, pos);
    }

    virtual void _p_remove(int idx) Q_DECL_OVERRIDE
    {
        remove(idx);
    }

    virtual void _p_purge(QObject* obj) Q_DECL_OVERRIDE
    {
        int index = m_items.indexOf(static_cast<T*>(obj));
        while (index != -1) {
            beginRemoveRows(QModelIndex(), index, index);
            m_items.removeAt(index);
            endRemoveRows();
            index = m_items.indexOf(static_cast<T*>(obj));
        }
    }

private:
    QList<T*> m_items;
};

#endif // QQMLOBJECTLIST_H
